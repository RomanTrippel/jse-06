package ru.trippel.tm.enumeration;

public enum  TypeRole {

    ADMIN("Admin"),
    USER("User");

    private String description = "";

    TypeRole(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
