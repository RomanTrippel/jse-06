package ru.trippel.tm.repository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository <T>{

    protected Map<String, T> map = new LinkedHashMap<>();

    public abstract List<T> findAll();

    public abstract T findOne(String id);

    public abstract T persist(T t);

    public abstract T merge(T t);

    public abstract T remove(String id);

    public abstract void removeAll();

}
