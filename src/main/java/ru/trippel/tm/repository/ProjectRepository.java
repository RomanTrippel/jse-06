package ru.trippel.tm.repository;

import ru.trippel.tm.entity.Project;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> {

    @Override
    public List<Project> findAll() {
        return new LinkedList<Project>(map.values());
    }

    public List<Project> findAll(String userId) {
        LinkedList<Project> listTemp = new LinkedList<>();
        LinkedList<Project> listAll  = new LinkedList<Project>(map.values());
        for (Project project: listAll) {
            if (project.getUserId().equals(userId)) listTemp.add(project);
        }
        return listTemp;
    }

    @Override
    public Project findOne(String id) {
        if (!map.containsKey(id)) return null;
        return map.get(id);
    }

    @Override
    public Project persist(Project project) {
        String projectId = project.getId();
        if (map.containsKey(projectId)) return null;
        return map.put(projectId,project);
    }

    @Override
    public Project merge(Project project) {
        String projectId = project.getId();
        if (!map.containsKey(projectId)) return null;
        return map.merge(projectId, project, (oldProject, newProject) -> newProject);
    }

    @Override
    public Project remove(String id) {
        if (!map.containsKey(id)) return null;
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
