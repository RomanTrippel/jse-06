package ru.trippel.tm.repository;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task> {

    @Override
    public List<Task> findAll() {
        return new LinkedList<Task>(map.values());
    }

    public List<Task> findAll(String userId) {
        LinkedList<Task> listTemp = new LinkedList<>();
        LinkedList<Task> listAll  = new LinkedList<Task>(map.values());
        for (Task task: listAll) {
            if (task.getUserId().equals(userId)) listTemp.add(task);
        }
        return listTemp;
    }

    @Override
    public Task findOne(String id) {
        if (!map.containsKey(id)) return null;
        return map.get(id);
    }

    @Override
    public Task persist(Task task) {
        String taskId = task.getId();
        if (map.containsKey(taskId)) return null;
        return map.put(taskId,task);
    }

    @Override
    public Task merge(Task task) {
        String taskId = task.getId();
        if (!map.containsKey(taskId)) return null;
        return map.merge(taskId, task, (oldProject, newProject) -> newProject);
    }

    @Override
    public Task remove(String id) {
        if (!map.containsKey(id)) return null;
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

    public void removeAll(String projectId) {
        LinkedList<Task> listAll  = new LinkedList<Task>(map.values());
        for (Task task: listAll) {
            if (task.getProjectId().equals(projectId)) map.remove(task.getId());
        }
    }

}
