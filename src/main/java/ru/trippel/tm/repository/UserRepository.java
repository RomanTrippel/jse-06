package ru.trippel.tm.repository;

import ru.trippel.tm.entity.User;
import java.util.LinkedList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> {

    @Override
    public List<User> findAll() {
        return new LinkedList<User>(map.values());
    }

    @Override
    public User findOne(String id) {
        if (!map.containsKey(id)) return null;
        return map.get(id);
    }

    @Override
    public User persist(User user) {
        String userId = user.getId();
        if (map.containsKey(userId)) return null;
        return map.put(userId,user);
    }

    @Override
    public User merge(User user) {
        String userId = user.getId();
        if (!map.containsKey(userId)) return null;
        return map.merge(userId, user, (oldProject, newProject) -> newProject);
    }

    @Override
    public User remove(String id) {
        if (!map.containsKey(id)) return null;
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
