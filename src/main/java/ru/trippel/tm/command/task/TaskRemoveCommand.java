package ru.trippel.tm.command.task;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_REMOVE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove a task.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        String taskId;
        int taskNum = -1;
        Task task;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Remove:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        taskId = taskList.get(taskNum).getId();
        bootstrap.getTaskService().remove(taskId);
        System.out.println("The task has been deleted.");
    }

}
