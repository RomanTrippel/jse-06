package ru.trippel.tm.command.task;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a task.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        Task task;
        String taskId;
        String newName;
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i + 1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        taskId = taskList.get(taskNum).getId();
        System.out.println("Enter new Name:");
        newName = KeyboardView.read();
        task = taskService.findOne(taskId);
        task.setName(newName);
        taskService.merge(task);
        System.out.println("Changes applied.");
    }

}
