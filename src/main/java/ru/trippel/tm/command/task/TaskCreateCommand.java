package ru.trippel.tm.command.task;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "TASK_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        Task task = new Task();
        String userId = bootstrap.getCurrentUser().getId();
        System.out.println("Enter task name.");
        String name = KeyboardView.read();
        task.setName(name);
        task.setUserId(userId);
        taskService.persist(task);
        System.out.println("The task \"" + task.getName() + "\" added!");
    }

}
