package ru.trippel.tm.command.project;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectAttachTaskCommand extends AbstractCommand {

    public ProjectAttachTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_ATTACHTASK";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Attach Task to Project.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Project> projectList = bootstrap.getProjectService().findAll(userId);;
        List<Task> taskList = bootstrap.getTaskService().findAll(userId);;
        String projectId;
        String taskId;
        Task task;
        int projectNum = -1;
        int taskNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getId();
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a project number");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        taskId = taskList.get(taskNum).getId();
        task = taskService.findOne(taskId);
        task.setProjectId(projectId);
        System.out.println("The task attached.");
    }

}
