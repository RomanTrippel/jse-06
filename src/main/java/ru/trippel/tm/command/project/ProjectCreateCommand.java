package ru.trippel.tm.command.project;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        String userId = bootstrap.getCurrentUser().getId();
        System.out.println("Enter project name.");
        String name = KeyboardView.read();
        project.setName(name);
        project.setUserId(userId);
        projectService.persist(project);
        System.out.println("The project \"" + project.getName() + "\" added!");
    }

}
