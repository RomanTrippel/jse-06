package ru.trippel.tm.command.project;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_REMOVE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove a project.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        String projectId;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Delete:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getId();
        bootstrap.getProjectService().remove(projectId);
        bootstrap.getTaskService().removeAll(projectId);
        System.out.println("The project has been deleted.");
    }

}
