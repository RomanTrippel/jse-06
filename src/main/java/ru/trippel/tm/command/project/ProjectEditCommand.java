package ru.trippel.tm.command.project;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a project.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        Project project;
        String projectId;
        String newName;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Edit:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getId();
        System.out.println("Enter new Name:");
        newName = KeyboardView.read();
        project = projectService.findOne(projectId);
        project.setName(newName);
        projectService.merge(project);
        System.out.println("Changes applied.");
    }

}
