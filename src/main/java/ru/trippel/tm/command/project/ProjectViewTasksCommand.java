package ru.trippel.tm.command.project;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class ProjectViewTasksCommand extends AbstractCommand {

    public ProjectViewTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_VIEWTASK";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "View all attached tasks.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getCurrentUser().getId();
        List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        String projectId;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        projectId = projectList.get(projectNum).getId();
        for (int i = 0; i < taskList.size() ; i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                System.out.println(taskList.get(i).getName());
            }
        }
    }

}
