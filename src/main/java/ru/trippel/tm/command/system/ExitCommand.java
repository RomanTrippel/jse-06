package ru.trippel.tm.command.system;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "EXIT";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Exit the application.";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

}
