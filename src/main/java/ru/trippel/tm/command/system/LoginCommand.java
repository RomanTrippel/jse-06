package ru.trippel.tm.command.system;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPass;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public class LoginCommand extends AbstractCommand {

    public LoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "LOGIN";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Authorization, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        List<User> userList = userService.findAll();
        User userTemp = new User();
        String login;
        String password;
        String passwordHash;
        System.out.println("Enter login:");
        login = KeyboardView.read();
        userTemp.setLogin(login);
        System.out.println("Enter password:");
        password = KeyboardView.read();
        passwordHash = HashPass.getHash(password);
        userTemp.setPassword(passwordHash);
        for (int i = 0; i <userList.size(); i++) {
            if (userList.get(i).equals(userTemp)) {
                bootstrap.setCurrentUser(userList.get(i));
                System.out.println("You have successfully logged in.");
                return;
            }
        }
        if (bootstrap.getCurrentUser() == null) {
            System.out.println("You entered the login or password incorrectly.");
        }
    }

}
