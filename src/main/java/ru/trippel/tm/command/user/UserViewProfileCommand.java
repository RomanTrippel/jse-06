package ru.trippel.tm.command.user;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;

import java.util.List;

public class UserViewProfileCommand extends AbstractCommand {

    public UserViewProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "USER_VIEWPROFILE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "View profile.";
    }

    @Override
    public void execute() {
        User user;
        String id = bootstrap.getCurrentUser().getId();
        user = userService.findOne(id);
        System.out.println(user);
    }

}
