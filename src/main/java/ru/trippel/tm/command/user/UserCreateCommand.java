package ru.trippel.tm.command.user;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPass;
import ru.trippel.tm.view.KeyboardView;

public class UserCreateCommand extends AbstractCommand {

    public UserCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "USER_CREATE";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Create a user, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        User user = new User();
        System.out.println("Enter login name.");
        String name = KeyboardView.read();
        user.setLogin(name);
        System.out.println("Enter password.");
        String password = KeyboardView.read();
        String passwordHash = HashPass.getHash(password);
        user.setPassword(passwordHash);
        userService.persist(user);
        System.out.println("User \"" + user.getLogin() + "\" added!");
    }

}
