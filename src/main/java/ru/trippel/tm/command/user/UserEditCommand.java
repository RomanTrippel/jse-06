package ru.trippel.tm.command.user;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.view.KeyboardView;

public class UserEditCommand extends AbstractCommand {

    public UserEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "USER_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws Exception {
        User user;
        String userId;
        String newLogin;
        userId = bootstrap.getCurrentUser().getId();
        user = userService.findOne(userId);
        System.out.println("Enter new Login.");
        newLogin = KeyboardView.read();
        user.setLogin(newLogin);
        userService.merge(user);
        bootstrap.setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
