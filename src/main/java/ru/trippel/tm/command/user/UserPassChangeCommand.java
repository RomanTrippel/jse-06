package ru.trippel.tm.command.user;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPass;
import ru.trippel.tm.view.KeyboardView;

public class UserPassChangeCommand extends AbstractCommand {

    public UserPassChangeCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "USER_PASSCHANGE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        User user;
        String userId;
        String newPassword;
        String newPasswordHash;
        userId = bootstrap.getCurrentUser().getId();
        user = userService.findOne(userId);
        System.out.println("Enter new Password");
        newPassword = KeyboardView.read();
        newPasswordHash = HashPass.getHash(newPassword);
        user.setPassword(newPasswordHash);
        bootstrap.setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
