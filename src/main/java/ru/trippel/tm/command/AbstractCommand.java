package ru.trippel.tm.command;

import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.service.UserService;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    protected ProjectService projectService;

    protected TaskService taskService;

    protected UserService userService;

    protected TypeRole role;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        projectService = bootstrap.getProjectService();
        taskService = bootstrap.getTaskService();
        userService = bootstrap.getUserService();
        role = TypeRole.USER;
    }

    public abstract String getNameCommand();

    public abstract boolean secure ();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public TypeRole getRole() {
        return role;
    }

    public void setRole(TypeRole role) {
        this.role = role;
    }

}

