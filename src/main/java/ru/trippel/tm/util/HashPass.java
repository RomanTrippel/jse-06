package ru.trippel.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashPass {

    public static String getHash(String in) throws NoSuchAlgorithmException {
        String result = null;
        String salt = "соль в начале" + in + "соль в конце";
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.reset();
        digest.update(salt.getBytes());
        BigInteger bigInt = new BigInteger(1, digest.digest());
        result = bigInt.toString(16);
        return result;
    }

}
