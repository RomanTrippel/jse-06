package ru.trippel.tm.service;

import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.view.KeyboardView;

import java.io.IOException;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAll(String userId) {
        return taskRepository.findAll(userId);
    }

    public Task findOne(String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    public Task persist(Task task) {
        if (task == null);
        String id = task.getId();
        if (id.isEmpty()) return null;
        return taskRepository.persist(task);
    }

    public Task merge(Task task) {
        if (task == null);
        String id = task.getId();
        if (id.isEmpty()) return null;
        return taskRepository.merge(task);
    }

    public Task remove(String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return taskRepository.remove(id);
    }

    public void removeAll(String projectId) {
        if (projectId == null) return;
        if (projectId.isEmpty()) return;
        taskRepository.removeAll(projectId);
    }

}
