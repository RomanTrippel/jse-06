package ru.trippel.tm.service;

import ru.trippel.tm.entity.User;
import ru.trippel.tm.repository.UserRepository;

import java.util.List;

public class UserService {

private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    public User persist(User user) {
        if (user == null);
        String id = user.getId();
        if (id.isEmpty()) return null;
        return userRepository.persist(user);
    }

    public User merge(User user) {
        if (user == null);
        String id = user.getId();
        if (id.isEmpty()) return null;
        return userRepository.merge(user);
    }

    public User remove(String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return userRepository.remove(id);
    }

}
